#!/bin/bash
Titulo="monitor del servidor"
Plantilla=plantilla.html
Of=admin.html

function numCpus {
	cat /proc/cpuinfo | grep processor | wc -l
}

function usbPci {
	echo "bus USB"
	echo "======="
	lsusb
	echo 
	echo
	echo "bus PCI"
	echo "======="
	lspci
}

function usoDeDiscos {	
	df -h > discos.txt	
}

function usoDeRam {
	free -h > memoria.txt
}
function numeroDeUsuarios {
	NumUsers=$(who | wc -l) 
}
function servicios {
	service --status-all | grep + > servicios.txt 
}

function suma { 
   NUM=$(echo "$1 + $2" | bc)
    echo $1 + $2 = $NUM
}


numCpus
usoDeDiscos	
usoDeRam 
numeroDeUsuarios
servicios
# el siguiente sed no funciona si alguno de los servicios no está instalado, porque el comando status devuelve 
# varias líneas
sed -e "s/{{titulo}}/${Titulo}/" \
-e "/{{memoria}}/ r memoria.txt" \
-e "/{{servicios}}/ r servicios.txt" \
-e "s/{{numUsers}}/${NumUsers}/" \
-e '/{{discos}}/ r discos.txt' $Plantilla > $Of

